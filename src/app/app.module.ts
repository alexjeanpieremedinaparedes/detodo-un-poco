import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthModule } from './pages/auth/auth.module';
import { DemoMaterialModule } from './material-modules';
import { RouterModule } from '@angular/router';
import { SpinnerComponent } from './commons/components/spinner/spinner.component';


@NgModule({
  declarations: [AppComponent, SpinnerComponent],

  imports: [
    BrowserModule,
    BrowserAnimationsModule,

    // Rutas
    AppRoutingModule,
    DemoMaterialModule,

    // Modulos
    AuthModule,
    RouterModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
