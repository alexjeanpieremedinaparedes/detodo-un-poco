  import { NgModule } from '@angular/core';
  import { RouterModule, Routes } from '@angular/router';
  import { RegisterComponent } from './commons/components/register/register.component';
  import { LoginPageComponent } from './pages/auth/login-page/login-page.component';


  export const routes: Routes = [
    {
      path:'login',
      component:LoginPageComponent
    },
    {
      path:'register',
      component:RegisterComponent
    },
    {
      path:'dashboard', // dashboard -> ''
      loadChildren:() => import('./pages/pages.module').then( (m) => m.PagesModule)
    },

    {
      path:'',
      redirectTo:'login',
      pathMatch:'prefix',
    }
  ];

  @NgModule({
    imports: [RouterModule.forRoot(routes,{useHash:true})],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }
