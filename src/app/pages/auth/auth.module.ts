import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginPageComponent } from './login-page/login-page.component';
import { LoginRegisterComponent } from './login-register/login-register.component';
import { AuthComponentsModule } from 'src/app/commons/shared/auth-components.module';
import { DashboardComponetsModule } from 'src/app/commons/shared/dashboard.module';
import { RouterModule } from '@angular/router';
import { PagesModule } from '../pages.module';


@NgModule({

  declarations: [
    LoginPageComponent,
    LoginRegisterComponent
  ],

  imports: [
    CommonModule,
    AuthComponentsModule,
    DashboardComponetsModule,
    RouterModule,
    PagesModule
  ],

  exports:[
    LoginPageComponent,
    LoginRegisterComponent
  ]
})
export class AuthModule { }
