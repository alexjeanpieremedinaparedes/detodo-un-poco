import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DigimonsComponent } from './dashboard/digimons/digimons.component';
import { MedabotsComponent } from './dashboard/medabots/medabots.component';
import { PokemonsComponent } from './dashboard/pokemons/pokemons.component';
import { DashboardRoutingModule } from './pages-routing.module';
import { DashboardComponetsModule } from '../commons/shared/dashboard.module';
import { DemoMaterialModule } from '../material-modules';



@NgModule({
  declarations: [
    DashboardComponent,
    DigimonsComponent,
    MedabotsComponent,
    PokemonsComponent
  ],
  imports: [
    CommonModule,
    // Ruta
    DashboardRoutingModule,
    DashboardComponetsModule,
    DemoMaterialModule,


  ],


})
export class PagesModule { }
